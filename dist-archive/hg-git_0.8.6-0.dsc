Format: 3.0 (quilt)
Source: hg-git
Binary: mercurial-git
Architecture: all
Version: 0.8.6-0
Maintainer: Python Applications Packaging Team <python-apps-team@lists.alioth.debian.org>
Uploaders:  Javi Merino <vicho@debian.org>
Homepage: https://hg-git.github.io/
Standards-Version: 3.9.8
Vcs-Browser: https://anonscm.debian.org/viewvc/python-apps/packages/hg-git/trunk/
Vcs-Svn: svn://anonscm.debian.org/python-apps/packages/hg-git/trunk/
Testsuite: autopkgtest
Testsuite-Triggers: git, unzip
Build-Depends: debhelper (>= 9), python (>= 2.6.6-3~), dh-python, unzip, mercurial, python-dulwich, git
Package-List:
 mercurial-git deb vcs optional arch=all
Checksums-Sha1:
 b842be466190cbbf3efb964ec2964a5c4285e512 89380 hg-git_0.8.6.orig.tar.xz
 714e3ecbb49f8eb5516c1ff37833b43bc0a442c3 5140 hg-git_0.8.6-0.debian.tar.xz
Checksums-Sha256:
 f8aa36f0b9157fe1dd5bf941f31abef70f4b3b3f1bc820d3540f544d74ddc72c 89380 hg-git_0.8.6.orig.tar.xz
 896fe550e8884bbec947b2e29265c80ef5a35d7453e1c8c992e19db46a918359 5140 hg-git_0.8.6-0.debian.tar.xz
Files:
 44ff6da9292dd3a31759bd9644bbf0a4 89380 hg-git_0.8.6.orig.tar.xz
 a87df4143378827e9779685372b62fa7 5140 hg-git_0.8.6-0.debian.tar.xz
