# hg-git-debian

An unofficial rebuild of the mercurial-git package for Debian Stretch.

Tags: packaging


## Download

- [⬇️ mercurial-git_0.8.6-0_all.deb](dist-archive/mercurial-git_0.8.6-0_all.deb) *(45.28 KiB)*
- [⬇️ hg-git_0.8.6.orig.tar.xz](dist-archive/hg-git_0.8.6.orig.tar.xz) *(87.29 KiB)*
- [⬇️ hg-git_0.8.6-0_amd64.changes](dist-archive/hg-git_0.8.6-0_amd64.changes) *(1.68 KiB)*
- [⬇️ hg-git_0.8.6-0_amd64.buildinfo](dist-archive/hg-git_0.8.6-0_amd64.buildinfo) *(6.04 KiB)*
- [⬇️ hg-git_0.8.6-0.dsc](dist-archive/hg-git_0.8.6-0.dsc) *(1.14 KiB)*
- [⬇️ hg-git_0.8.6-0.debian.tar.xz](dist-archive/hg-git_0.8.6-0.debian.tar.xz) *(5.02 KiB)*
